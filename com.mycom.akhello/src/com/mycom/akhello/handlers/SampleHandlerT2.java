package com.mycom.akhello.handlers;
import com.mycom.akhello.actions.*;
/*
 import com.mycom.akhello.actions.FrameDS;

public class InitialiseFrameDS {
 */
import java.util.*;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.text.*;    // .TextSelection
import com.mycom.akhello.Activator;   

import com.teamcenter.rac.aif.AIFClipboard;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AIFPortal;
import com.teamcenter.rac.aif.AIFTransferable;
import com.teamcenter.rac.aif.AbstractAIFApplication;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.AIFKernel;
import com.teamcenter.rac.aifrcp.RCPPortal;
import com.teamcenter.rac.kernel.TCSession;
                 // ------------- test -------

// ------------ rcp portal
import com.teamcenter.rac.aifrcp.RCPPortal;
import com.teamcenter.rac.aif.kernel.AIFSessionManager;
import com.teamcenter.rac.aif.kernel.AIFKernel;
import com.teamcenter.rac.aifrcp.RCPPortal;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */

public class SampleHandlerT2 extends AbstractHandler{
//	org.eclipse.jface.viewers.StructuredViewer sw = null;

	AIFComponentContext context = null;
	Object [] obs = null;
	/**
	 * The constructor.
	 */
	public SampleHandlerT2 () {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		TCSession session = null;
		try {
			               // ============== for session
			session = (TCSession) Activator.getDefault().
			getSessionService().getSession(TCSession.class.getName());
			
			MessageDialog.openInformation(window.getShell(),
					"Tc8test Plug-in   - s-handler -- 12-50",
					"��� ������������: " + session.getUserName());
			if (session != null)
			 System.out.println ("SampleHandlerT2 ------- after getting session , session not null");
			else
		     System.out.println ("SampleHandlerT2 ------- after getting session , session is null");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		                // =============== for targets (treat different input classes
		StructuredSelection selection = null;
		
		Object ob = window.getActivePage().getSelection(); // just to check the class name
	    String s = ob.getClass().getName();
	        System.out.println ("SampleHandlerT2 -------class name =" + s);
	    
	    if (s.equals("org.eclipse.jface.text.TextSelection"))  {
			TextSelection ts = null;
	     	ts = (TextSelection)ob;
	          	 System.out.println ("SampleHandlerT2 -------selected text=" + ts.getText());
	          	 // �� ��� ����� ��������� �������� � ������-�� �������.
	    }
	    else if (s.equals("org.eclipse.jface.viewers.StructuredSelection") ||
	    		 s.equals("org.eclipse.jface.viewers.TreeSelection")) {
	    	      System.out.println ("SampleHandlerT2 ------ �������� ����� ������� - StructuredSelection, TreeSel__ ");
	    	   selection = (StructuredSelection) window.getActivePage().getSelection();
	   	       Object [] obs = selection.toArray();          // working
	   			  MessageDialog.openInformation(window.getShell(), " title test obs", 
		                 "obs q =" + obs.length);     // (parent, title, message)
	        }
	        else {
			       MessageDialog.openInformation(window.getShell(),
					"Tc8test Plug-in   - ���� ����� �� ��������������",
					"��� ������������: " + session.getUserName());
			   return null;
	        }
 		              // ------------- further treatment for targets
		if(selection != null && !selection.isEmpty()) {
			Object firstElement = selection.getFirstElement();
			if(firstElement instanceof AIFComponentContext) {
				context = (AIFComponentContext) firstElement;
				MessageDialog.openInformation(window.getShell(),
						"Tc8 test Plug-in, selected ---- 12-50",
					     context.getComponent().getType());
			   obs = selection.toArray();	
			}
		}  else {
			MessageDialog.openInformation(
					window.getShell(),
					"Tc8 test Plug-in  -- 12-50",
					"Nothing selected");
		}
		/*                // ---------- working
		            // ------------  how to get rcp Portal 
	    AbstractAIFApplication app = null;
	    app = AIFDesktop.getActiveDesktop().getCurrentApplication();
	    AIFKernel kernel =AIFDesktop.getActiveDesktop().getDesktopManager().getKernel();  
	    	   
	    RCPPortal rcpp = null;               
	   
        AIFClipboard cp =  null;  // cp.setContents.....  // depricated
             // --------- rcpPortal -------
        AIFSessionManager am = null;   // just for session man.
    
            // AIFKernel ker = new AIFKernel (rcpp);    // � ������.����� ������� �������� ��...
             
         AIFPortal por = kernel.getPortal();
       
         rcpp = (RCPPortal) por;    
    
        AIFClipboard clipboard = null;
        clipboard =  rcpp.getClipboard();
            
                  //  AIFTransferable tr = null;
        Vector vector = new Vector ();
        vector = clipboard.toVector(); 
   
		MessageDialog.openInformation(
				window.getShell(),
				"Tc8 test Plug-in  -- 13-50" + "vector size =" + vector.size(),
				"Nothing selected");	
		if (true) return null; // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		         // --------  // ------------  how to get rcp Portal  ----- finish
		*/
		
		            // ------------ call for creation ....
		StartLoadMi171 load = new StartLoadMi171 (session, context, obs);
		                                // following classes :     FrameLoadMi171 , LoadMi171
		
		// CreateIzd2 crIzd2 = new CreateIzd2 (session);  
		// InitialiseFrameDS frame = new InitialiseFrameDS ();
		
        // ---------- examples class names for targets
        // org.eclipse.jface.viewers.StructuredSelection - for PSe, Nav.
        // sometimes - ...viewers.TreeSelection - for Nav.
	    // org.eclipse.jface.viewers.TreeSelection
		// ISelection - interface (I... -?)
		       // ---------------------------------------------
		
		return null;
	}  // proc	
	
	
	
	
}
