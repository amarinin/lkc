package com.mycom.akhello.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;

import com.mycom.akhello.Activator;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCSession;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */

public class SampleHandlerT8 extends AbstractHandler{

	/**
	 * The constructor.
	 */
	public SampleHandlerT8 () {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		TCSession session;
		try {
			session = (TCSession) Activator.getDefault().
			getSessionService().getSession(TCSession.class.getName());
			MessageDialog.openInformation(window.getShell(),
					"Tc8test Plug-in",
					"��� ������������: " + session.getUserName());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		StructuredSelection selection = (StructuredSelection) window.getActivePage().getSelection();
		
		if(selection != null && !selection.isEmpty()) {
			Object firstElement = selection.getFirstElement();
			if(firstElement instanceof AIFComponentContext) {
				AIFComponentContext context = (AIFComponentContext) firstElement;
				MessageDialog.openInformation(window.getShell(),
						"Tc8test Plug-in",
						context.getComponent().getType());
			}
		}  else {
			MessageDialog.openInformation(
					window.getShell(),
					"Tc8test Plug-in",
					"Nothing selected");
		}
		
		return null;
	}	
	
	
	
	
}
