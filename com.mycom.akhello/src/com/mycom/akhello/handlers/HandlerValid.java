package com.mycom.akhello.handlers;
import lkc.*;
import java.io.Console;
import java.util.*;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.jobs.*;     // Job
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.*;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.text.*;    // .TextSelection
import org.eclipse.swt.widgets.Shell;

import com.mycom.akhello.Activator;

import com.teamcenter.rac.aif.AIFClipboard;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AIFPortal;
import com.teamcenter.rac.aif.AIFTransferable;
import com.teamcenter.rac.aif.AbstractAIFApplication;
import com.teamcenter.rac.aif.kernel.*;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.aifrcp.RCPPortal;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.stylesheet.XMLRendering.ViewsCheckBoxMenuItem;
// ------------- test -------
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;

// ------------ rcp portal
import com.teamcenter.rac.aifrcp.RCPPortal;
import com.teamcenter.rac.aif.kernel.AIFSessionManager;
import com.teamcenter.rac.aif.kernel.AIFKernel;
import com.teamcenter.rac.aifrcp.RCPPortal;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.PlatformUI;
/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
     // CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
public class HandlerValid extends AbstractHandler{
         //	org.eclipse.jface.viewers.StructuredViewer sw = null;
	AIFComponentContext context = null;
	Object [] obs = null;
	boolean PRINT = true;
	boolean PRINT_M = true;
	IWorkbenchWindow window = null;
	TCSession session = null;
	/**
	 * The constructor.
	 */
      // ****************************************     K     K     K
	public HandlerValid () {     
	}
	/**
	 * the command has been executed, so extract  the needed information
	 * from the application context.
	 */
	  // ********************************************************   EXECUTE
	public Object execute(ExecutionEvent event) throws ExecutionException {
		window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		try {
			     // ============== for session
	    	session = (TCSession) Activator.getDefault().
			getSessionService().getSession(TCSession.class.getName());
			
			   // MessageDialog.openInformation(window.getShell(),      // lmg 31 07 13
			   // 		"Tc8test Plug-in   - s-handler -- - 24 --- 05  12",
			   //	"��� ������������: " + session.getUserName() + "/ window class = " + window.getClass().getName());
			if (session != null)
			 System.out.println ("HandlerSearchNR4fs ------- after getting session , session not null");
			else
		     System.out.println ("HandlerSearchNR4fs ------- after getting session , session is null");
		} catch (Exception e) {
			// TODO Auto-generated catch block
		    	e.printStackTrace();
		}
		                // =============== for targets (treat different input classes
		StructuredSelection selection = null;
		Object ob = window.getActivePage().getSelection(); // just to check the class name
	    String s = ob.getClass().getName();
	        System.out.println ("HandlerSearchNR4fs -------class name =" + s);
	    
	    if (s.equals("org.eclipse.jface.text.TextSelection"))  {
			TextSelection ts = null;
	     	ts = (TextSelection)ob;
	          	 System.out.println ("HandlerSearchNR4fs -------selected text=" + ts.getText());
	          	 // �� ��� ����� ��������� �������� � ������-�� �������.
	    }
	    else if (s.equals("org.eclipse.jface.viewers.StructuredSelection") ||
	    		 s.equals("org.eclipse.jface.viewers.TreeSelection")) {
	    	      System.out.println ("SampleHandlerT2 ------ �������� ����� ������� - StructuredSelection, TreeSel__ ");
	    	   selection = (StructuredSelection) window.getActivePage().getSelection();
	   	       Object [] obs = selection.toArray();                 // working
  	   			  //  MessageDialog.openInformation(window.getShell(), " title test obs",   // lmg 31 07 13
		           //         "obs q =" + obs.length);     // (parent, title, message)
	         } else {
			       MessageDialog.openInformation(window.getShell(),
					"Tc9 test Plug-in   - ���� ����� �� ��������������",
					"��� ������������: " + session.getUserName());
			        return null;
	           }
 		          // ------------- further treatment for targets
		if(selection != null && !selection.isEmpty()) {
			Object firstElement = selection.getFirstElement();
			if(firstElement instanceof AIFComponentContext) {
				context = (AIFComponentContext) firstElement;
			    if (PRINT_M)  MessageDialog.openInformation(window.getShell(),
						"Tc9 test Plug-in, selected ---- 24 --- 05  12, Type = ",
					     context.getComponent().getType());
			    obs = selection.toArray();	
			}
		} else {
			 if (PRINT_M) MessageDialog.openInformation(	window.getShell(),
					   "Tc9 test Plug-in  -- - 24 --- 05  12",	"Nothing selected");
		   }
		              // ------------ class test ----------------- start
		/*
		try {
			AIFComponentContext aifcc =  new AIFComponentContext (null, (InterfaceAIFComponent) context.getComponent(), " ");
		    TCComponent tcc = (TCComponent) aifcc.getComponent();	
		    String sTCClass1 = tcc.getTCClass().getClassName();
		    
		    String sTCClass2 = tcc.getClassType().getClass().getName();	
		       System.out.println ("SampleHandlerT2 ========= TCClass / TCType =" +sTCClass1 + "/" + sTCClass2);
		       System.out.println ("SampleHandlerT2 ========= 17 01 -- class = " +  tcc.getTypeObject().getClassName());
		    if (tcc instanceof TCComponentItem)   
		    	System.out.println ("SampleHandlerT2 ========= tcc is instance of TCComponentItem" );
		    else System.out.println ("SampleHandlerT2 ========= tcc is no instance of TCComponentItem" );
	               // ------------ class test --------------  end
		} catch (TCException e) { e.printStackTrace();  }
		     System.out.println ("SampleHandlerT2 ========= before start of load" );
            */
			       // -------- ���� ������� ���.���� ������ ---------- 
		     // **************  CCCCCCCCCCCCCCCCCCCCCC - start
		     Job job = new Job("My First Job") {
		         protected IStatus run(IProgressMonitor monitor) {
		                 System.out.println("Hello World (from a background job)");
		              if (window == null)   System.out.println("SampleHandler **** window = null");
		              Display display = window.getShell().getDisplay();
		              StartValid load = new StartValid (session, context, obs, window); 
		                     // -----------------
		          
		                     // -----------------
		                 return  Status.OK_STATUS;
		            }
		         };   // ----- class job
			     // **************  CCCCCCCCCCCCCCCCCCCCCC - end
		      job.setPriority(Job.SHORT);
		      job.setUser(true);
		      job.schedule();                   // start as soon as possible
		      
		                // -------- ���� ������� ���.���� ������ -------  end
		//   StartLoadMta load = new StartLoadMta (session, context, obs, window); // >>>>>>>>>>>>
		       // ---------------------------------------------
		return null;
	}  // proc	
      // ***************************************************
 void test () {
	ViewPart viewPart = null;
	IWorkbenchWindow window = null;
    // window.getActivePage().getActivePart().
	PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView("customproduct.result");
    IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
    if (!page.isPartVisible(viewPart)) page.bringToTop(viewPart);
	}
	// ****************************************************
void delayedPrint (String title) {
	int count = 0;
	do {                  // VVVVVVVVVVVVVVVVV
		  System.out.println(" delayed print--------- "  + title + " / " + count);
		           // --------  ���� ��������
	    for (int j =0; j < 1000000; j ++)  {
		        //  session.setStatus("print ********************" );
	     }    // for
	    count ++; 
	} while (true);        // AAAAAAAAAAAAAAA
}   // proc
     // *******************************************
Runnable progress = new Runnable() {
    public void run () {
    	delayedPrint (" 8888888888 ---");
    }
};
   // ************************************	
}    // class - main