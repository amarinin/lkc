package lkc;
import com.teamcenter.rac.kernel.TCComponent;

public class ReportData {
    public String id = new String("");
    public String name = new String("");
    public String type = new String("");
    public String ver = new String("");
    public String csum = new String("");
    public String date = new String("");
    public String descr = new String("");
    public String subname = new String("");
    public String subid = new String("");
    public String subtype = new String("");
    public String dsVer = new String("");
}

class DataRoot {
    public String id = new String("");
    public String type = new String("");
    public String object_type = new String("");
    public String name = new String("");
    public String ver = new String("");
    public String tree = new String("");
    public DataAttr[] da;
}

class DataAttr {
    public String id = new String("");
    public String tn = new String("");
    public String type = new String("");
    public String name = new String("");
    public String csum = new String("");
    public String file = new String("");
    public TCComponent comp = null;
    public String dsVer = new String("");
}
/*
class DataULdlg {
    // IMANSession session = null;
	boolean okFlag = false;
    int SELECT_CHANGE = 0;
    String query = new String ("");           // CodeIzd
    String topIzdItemId = new String (""); 
    String topIzdRevId = new String (""); 
    String topIzdItemRevId = new String (""); 
    // InterfaceAIFComponent[] targets = null;
    String izdelie = new String ("");         // setNameIzdelie
    String numberyl = new String (""); 
    // String numbersz = new String ("");   
    // String massa = new String ("");            // MASSA
    // String izmmassa = new String ("");         // IZM_MASSA   
    String vypolnil = new String (""); 
    String proveril = new String ("");  
    // String assem = new String ("");   
    int SELECT_STATUS = 0;
    boolean IS_CREATE_IZD = false;
    boolean IS_CREATE_MASTERFORM = false;
    String sRule = new String (""); 
    String TEST_ITEM_ID = new String ("");    

}   // class
*/
