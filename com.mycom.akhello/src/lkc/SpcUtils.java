package lkc;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lgnetov
 */
import com.teamcenter.rac.kernel.*;
/*
import com.ugsolutions.iman.kernel.IMANComponent;
import com.ugsolutions.iman.kernel.IMANComponentItemType;
import com.ugsolutions.iman.kernel.IMANException;
import com.ugsolutions.iman.kernel.IMANReservationService;
import com.ugsolutions.iman.kernel.IMANSession;
*/
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SpcUtils {

    /**
     * ������� �������� �� ����������������� ����������
     * @param ic
     * @param session
     * @return
     * @throws IMANException 
     */
    public static boolean isReservation(TCComponent ic, TCSession session) throws TCException {
        if (ic == null) return false;        
        TCReservationService reservation = session.getReservationService();
        if (!reservation.isReserved(ic)) return true;        
        return false;
    }

    public static boolean isStatus(TCComponent ic) throws TCException {
    	TCComponent[] rStatus = ic.getRelatedComponents("release_status_list");
        for (int i = 0; i < rStatus.length; i++) {
            String effName = rStatus[i].getTCProperty("name").getStringValue();
            if (effName.equals("����������")) return true;            
        }
        return false;
    }
    
    public static boolean isStartUser(TCComponent ic, String startUser) throws TCException { 
        String value = ic.getReferenceProperty("owning_user").toString();
        value = value.substring( value.indexOf("(")+ 1, value.indexOf(")") );
        if (value.equals(startUser)) return true;       
        else return false;
    }
    
    public static boolean isSZ(String sz, TCSession session) throws TCException{
    	TCComponentItemType itemType = (TCComponentItemType) session.getTypeComponent("Item");
    	TCComponent component = (TCComponent) itemType.find(sz);
        if (component == null) return false;
        else return true;
    }
    
    public static String now(String dateFormat) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(calendar.getTime());
    }
}
