
package lkc;

import java.util.*;

import javax.swing.JFrame;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.export.*;
//import org.apache.commons.beanutils.locale.LocaleConverter;
//import org.apache.commons.logging.LogFactory;
import net.sf.jasperreports.swing.JRViewer;

/**
 * ����� �������� ������ ����� jasperreport
 * @author AMarinin
 * 16.01.14.
 */
public class CreateReport {
	private String templateName;
    private String xmlFileName;
	private String recordPath;
    private String outFileName;
        
    private HashMap hm = new HashMap();
    private JasperPrint print;
    
    /**
     * ����������� �� ���������
     */
    public CreateReport() {}
    
    /**
     * ����������� �������� ������
     * @param templateName - ���� � ������� (*.jasper)
     * @param xmlFileName - ���� � ����� � ������� (*.xml)
     * @param recordPath ���� � ������ � ����� xml-����� (XPath)
     
     */
    public CreateReport(String templateName, String xmlFileName, String recordPath){
    	this.templateName = templateName;
    	this.xmlFileName = xmlFileName;
    	this.recordPath = recordPath;
    	//this.outFileName = outFileName;
    	create();
    }
    
    // main ������ ��� ������������
    public static void main(String[] args){
    	CreateReport r = new CreateReport("/home/amarinin/report4.jasper",
    			                          "/home/amarinin/projects/compLkc/wfiles/d_udlist.xml",
    			                          "/udlist/body/elem");
    	r.openReport();
    }
    
    /**
     * ������� ������� ���� �� �� ��������� ��� � �� �������
     */
    public void create(){
    
	    try {
	    	
	    	JRXmlDataSource jrxmlds = new JRXmlDataSource(xmlFileName, recordPath);
	    		    	
	        // Fill the report using an empty data source
	        //JasperPrint print = JasperFillManager.fillReport(fileName, hm, new JREmptyDataSource());
	    	print = JasperFillManager.fillReport(templateName, hm, jrxmlds);
		
	    } catch (JRException e) {
	        System.out.println("��� �������� ������ ��������� ������");
	    	e.printStackTrace();
	    //} catch (Exception e) {
	    //    e.printStackTrace();
	        
	    }
    }
    
    /**
     * @param outFileName - ���� �� �������� ����� �������� ����� (*.pdf)
     * ������� ��������� ����� � ���� *.pdf
     */
    public void saveToPdf(String outFileName){
    	// Create a PDF exporter
        JRExporter exporter = new JRPdfExporter();

        // Configure the exporter (set output file name and print object)
        exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outFileName);
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
    
        // Export the PDF file
        try {
			exporter.exportReport();
			System.out.println("����� � ������� Pdf ������");
		} catch (JRException e) {
			// TODO Auto-generated catch block
			System.out.println("��� �������� ������ � ������� Pdf ��������� ������");
			e.printStackTrace();
		}
    }
    
    /**
     * ������� ��������� ���� ��������� ������, ������� ��������� ��������� ����� � ������ ��������
     */
    public void openReport(){
    	//�������� ������ � ��������� ����
        JFrame frame = new JFrame("Report");
        JRViewer jrViewer = new JRViewer(print);
        frame.getContentPane().add(jrViewer);
        frame.pack();
        frame.setSize(400,600);
        frame.setVisible(true);
    }
    
    /**
     * @param String templaneName
     * ������� ������������� ���� � ������� (*.jasper)
     */
    public void setTemplateName(String templateName){
    	this.templateName = templateName;
    }
    
    /**
     * @param String xmlFileName
     * ������� ������������� ���� � ����� � ������� (*.xml)
     */
    public void setXmlFileName(String xmlFileName){
    	this.xmlFileName = xmlFileName;
    }
    
    /**
     * @param String recordPath
     * ������� ������������� ���� � ������ � ����� xml-����� (XPath)
     */
    public void setRecordPath(String recordPath){
    	this.recordPath = recordPath;
    }
    
    ///**
    // * @param String outFileName
    // * ������� ������������� ���� �� �������� ����� �������� ����� (*.pdf)
    // */
    //public void setOutFileName(String outFileName){
    //	this.outFileName = outFileName;
    //}
    

}
