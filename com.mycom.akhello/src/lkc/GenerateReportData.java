package lkc;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author KMuhov
 */
public class GenerateReportData {

    private DocumentBuilder builder = null;
    private Document document = null;
    private Element root = null;

    public String full_noname = "";
    public String razrab = "";
    public String prover = "";
    public String tkont = "";
    public String nachotdela = "";
    public String nkontr = "";
    public String ytv = "";
    public String vypolnil = "";
    public String proveril = "";
    public String numbersz = "";
    public String litera = "";
    public String izdelie = "";
    public String numberyl = "";
    public String assem = "";
    public String rule = "";
    public String filter = "";
    public String change = "";
    public String ver = "";
    public String generate_date_now = "";
 
    public boolean ERROR;

    public HashMap hash_data = new HashMap();  // hash_data - ������ ���� - ReportData
    public String DIR_PATH = "";

    private void initParams() {
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = builder.newDocument();
            root = document.createElement("udlist");
            document.appendChild(root);
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    private void createTitle() {
        Element title = document.createElement("title");
        root.appendChild(title);

        Element e_er = document.createElement("er");
        e_er.appendChild(document.createTextNode(ERROR ? "���� ��� �������� ������!" : ""));
        title.appendChild(e_er);

        Element e_noname = document.createElement("noname");
        e_noname.appendChild(document.createTextNode("���"));
        title.appendChild(e_noname);

        Element e_full_noname = document.createElement("full_noname");
        e_full_noname.appendChild(document.createTextNode(full_noname));
        title.appendChild(e_full_noname);
    }

    private void createHead(String number_yl) {
        Element head = document.createElement("head");
        root.appendChild(head);

        Element e_razrab = document.createElement("razrab");
        e_razrab.appendChild(document.createTextNode(razrab));
        head.appendChild(e_razrab);

        Element e_prover = document.createElement("prover");
        e_prover.appendChild(document.createTextNode(prover));
        head.appendChild(e_prover);

        Element e_tkont = document.createElement("tkont");
        e_tkont.appendChild(document.createTextNode(tkont));
        head.appendChild(e_tkont);

        Element e_nachotdela = document.createElement("nachotdela");
        e_nachotdela.appendChild(document.createTextNode(nachotdela));
        head.appendChild(e_nachotdela);

        Element e_nkontr = document.createElement("nkontr");
        e_nkontr.appendChild(document.createTextNode(nkontr));
        head.appendChild(e_nkontr);

        Element e_ytv = document.createElement("ytv");
        e_ytv.appendChild(document.createTextNode(ytv));
        head.appendChild(e_ytv);

        Element e_vypolnil = document.createElement("vypolnil");
        e_vypolnil.appendChild(document.createTextNode(vypolnil));
        head.appendChild(e_vypolnil);

        Element e_proveril = document.createElement("proveril");
        e_proveril.appendChild(document.createTextNode(proveril));
        head.appendChild(e_proveril);

        Element e_numbersz = document.createElement("numbersz");
        e_numbersz.appendChild(document.createTextNode(numbersz));
        head.appendChild(e_numbersz);

        Element e_litera = document.createElement("litera");
        e_litera.appendChild(document.createTextNode(litera));
        head.appendChild(e_litera);

        Element e_izdelie = document.createElement("izdelie");
        e_izdelie.appendChild(document.createTextNode(izdelie));
        head.appendChild(e_izdelie);

        Element e_numberyl = document.createElement("numberyl");
        e_numberyl.appendChild(document.createTextNode(number_yl));
        head.appendChild(e_numberyl);

        Element e_assem = document.createElement("assem");
        e_assem.appendChild(document.createTextNode(assem));
        head.appendChild(e_assem);
 
        Element e_generate_date_now = document.createElement("generate_date_now");
        e_generate_date_now.appendChild(document.createTextNode(generate_date_now));
        head.appendChild(e_generate_date_now);
    }

    private void createOptions() {
        Element options = document.createElement("options");
        root.appendChild(options);

        Element e_rule = document.createElement("rule");
        e_rule.appendChild(document.createTextNode(rule));
        options.appendChild(e_rule);

        Element e_filter = document.createElement("filter");
        e_filter.appendChild(document.createTextNode(filter));
        options.appendChild(e_filter);

        Element e_change = document.createElement("change");
        e_change.appendChild(document.createTextNode(change));
        options.appendChild(e_change);

        Element e_ver = document.createElement("ver");
        e_ver.appendChild(document.createTextNode(ver));
        options.appendChild(e_ver);
    }

    private void createBody() {
        Element body = document.createElement("body");
        root.appendChild(body);

        Iterator iterator = hash_data.keySet().iterator();  
        int pos = 0;
        int length = 37;
        HashMap dictionary = new HashMap();   // ��� ������� ������ ��� ��.������� ������ - rd.id
                          // VVVVVVVVVVVVVVVVVV  - ���. ����
        while (iterator.hasNext()) {
            String key = iterator.next().toString();
            ReportData rd = (ReportData) hash_data.get(key);
            concatDataEtd(body, rd, pos, length, dictionary);    // >>>>>>>>>>
            pos++;
        }   // while     // AAAAAAAAAAAAAAAAAAAA
    }
                // ��������� ����� ��. ������. �������� ���������� ��
    private void concatDataEtd(Element body, ReportData rd, int pos, int length, HashMap dictionary) {
        if (rd.name.length() > length && !dictionary.containsKey(rd.id)) {
            Vector vector = GlobalVars.getDisjointString(rd.name, length); 
                      // disjoint ������ ��� name   // ... � ������ �� ���� ���������� ������
            boolean is_first = true;
            for (int i = 1; i < vector.size(); i++) {
                if (is_first) {
                    createDataAttrs(body, pos, i-1, rd.id, rd.name, vector.get(0).toString(), rd.type, rd.ver, "", "", "", vector.get(i).toString(), rd.dsVer);
                    is_first = false;
                } else createDataAttrs(body, pos, i-1, rd.id, rd.name, "", rd.type, rd.ver, "", "", "", vector.get(i).toString(), "");
            }
            dictionary.put(rd.id, "");
        }
        createDataAttrs(body, pos, 1, rd.id, rd.name, rd.name, rd.type, rd.ver, rd.csum, rd.subid, rd.subtype, rd.subname, rd.dsVer);
    }
                    // - ������ ������ ���� - elem
    private void createDataAttrs(Element body, int pos, int grpos, String id,
            String name, String group_name, String type, String ver,
            String csum, String sub_id, String sub_type, String sub_name, String dsVer) {
        Element element = document.createElement("elem");
        element.setAttribute("group", ""+pos);
        element.setAttribute("sub_group", ""+grpos);
        element.setAttribute("id", id);
        element.setAttribute("name", name);
        element.setAttribute("group_name", group_name);
        element.setAttribute("type", type);
        element.setAttribute("ver", ver);
        element.setAttribute("csum", csum);
        element.setAttribute("sub_id", sub_id);
        element.setAttribute("sub_type", sub_type);
        element.setAttribute("sub_name", sub_name);
        element.setAttribute("dsVer", dsVer);
        body.appendChild(element);
    }

    private void writeXmlFile() {
        try {
            File file = new File(DIR_PATH);
            Source source = new DOMSource(document);
            Result result = new StreamResult(file);
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty(OutputKeys.ENCODING, "windows-1251");
            xformer.setOutputProperty(OutputKeys.INDENT, "yes");
            xformer.transform(source, result);
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    public void constructXmlData(String number_yl) {
        initParams();
        createTitle();
        createOptions();
        createHead(number_yl);
        createBody();
        writeXmlFile();
    }
}