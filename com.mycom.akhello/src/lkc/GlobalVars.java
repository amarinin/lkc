package lkc;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

/**
 *
 * @author KMuhov
 * @author amarinin
 */
public class GlobalVars {

    private static String PATH = "C:\\rpc\\compLkc\\wfiles\\save";

    public static void saveGlobalVars(String vypolnil, String proveril, String izd,
            String yl, String number_yl, String assem, String number_sz, String mass,
            String izm_mass) {
        try {
            OutputStreamWriter oswFile = new OutputStreamWriter(new FileOutputStream(PATH), "Cp1251");
            oswFile.write(vypolnil + "\r\n");
            oswFile.write(proveril + "\r\n");
            oswFile.write(izd + "\r\n");
            oswFile.write(yl + "\r\n");
            oswFile.write(number_yl + "\r\n");
            oswFile.write(assem + "\r\n");
            oswFile.write(number_sz + "\r\n");
            oswFile.write(mass + "\r\n");
            oswFile.write(izm_mass + "\r\n");
            oswFile.close();
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    public static String[] readGlobalVars() {
        String[] arrVars = {"", "", "", "", "", "", "", "", ""};
        String line = "";

        try {
            BufferedReader readFile = new BufferedReader(new FileReader(PATH));
            int counter = 0;
            while ((line = readFile.readLine()) != null) {
                arrVars[counter] = line;
                counter++;
            }
        } catch (Exception ex) { ex.printStackTrace(); }
        return arrVars;
    }

    public static Vector getDisjointString(String name, int length) {
        boolean end = false;
        Vector v = new Vector();
        while (!end) {
            if (name.length() <= length) {
                end = true;
                v.add(name);
                break;
            }

            char[] arr = name.toCharArray();
            String buff = name.substring(0, length);
            char[] mid = buff.toCharArray();

            int len = length;
            if (!Character.toString(mid[length-1]).equals(" ") && !Character.toString(arr[length]).equals(" ")) {
                for (int i = mid.length - 1; i >= 0; i--) {
                    if (Character.toString(mid[i]).equals(" ")) {
                        len = i + 1;
                        break;
                    }
                }
            }

            v.add(name.substring(0, len));
            name = name.substring(len).trim();
        }

        return v;
    }

    public static void printHash(HashMap hErrEltMap) {
        Iterator iterator = hErrEltMap.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next().toString();
            ReportData rd = (ReportData) hErrEltMap.get(key);
            System.out.println("    key#"+key+";   value#"+rd.id+"/"+rd.ver+"-"+rd.name+"   type#"+rd.type+rd.subname+":"+rd.subtype);
        }
        System.out.println("\n");
    }

    public static long getLongValue(String str_value) {
        try { return Long.parseLong(str_value, 16);
        } catch (Exception ex) { return 0; }
    }

    public static void getIdName(DataAttr da, String str_type, String name, HashMap hmd) {
        /*if (str_type.equalsIgnoreCase("UGPART")) {
          int index = name.indexOf("/");
          String name_not_rev = index != -1 ? name.substring(0, index) : name;
            String[] array = name_not_rev.split("\\.");
            String r = "";
            if (name != null && array.length == 5) {
                if (array[4].startsWith("56") || array[4].startsWith("57")) r = "������";
                else r = "������";
            }
            if (name != null && name_not_rev != null)
              r = name_not_rev.endsWith(".56") || name_not_rev.endsWith(".57") ? "������" : "������";

            da.id = r;
            da.type = str_type;
            da.tn = str_type;
        } else {*/
            NData nd =    hmd.containsKey(str_type) ? (NData) hmd.get(str_type) : null;
            da.id =     nd == null ? "" : nd.id;
            da.type = str_type;        //nd.yl;
            da.tn = str_type;
        //}
    }
    
    public static boolean isWindows(){

        String os = System.getProperty("os.name").toLowerCase();
        //windows
        return (os.indexOf( "win" ) >= 0); 

    }
    
    public static boolean isMac(){

        String os = System.getProperty("os.name").toLowerCase();
        //Mac
        return (os.indexOf( "mac" ) >= 0); 

    }

    public static boolean isUnix (){

        String os = System.getProperty("os.name").toLowerCase();
        //linux or unix
        return (os.indexOf( "nix") >=0 || os.indexOf( "nux") >=0);

    }
    
    
}
