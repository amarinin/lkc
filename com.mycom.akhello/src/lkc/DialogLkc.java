package lkc;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
     // CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC class
public class DialogLkc extends Dialog {

	protected Object result = null;
	protected Shell shell;
	public DataULdlg dud = null;
	Button check1 = null;
	Button check2 = null;
	
	Button btn1 = null;
	Button btn2 = null;

	private Text tfCodeIzd;
	private Text tfCodeRevIzd;
	private Text tfRazrab;
	private Text tfLkcIzdNum;
	private Text tfLkcRevIzdNum;
	private Text tfIzdelieMC = null;
	private Combo comboRR;
	
	Button radioButton1 = null;
	Button radioButton2 = null;
	Button radioButton3 = null;
	
	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	  // ********************************************* K K K
	public DialogLkc (Shell parent, int style, DataULdlg theDud) {
		super(parent, style);
		setText("SWT Dialog");
		dud = theDud;        
	    open (); 
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	  // ***************************************
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	   // *************************************
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(450, 571);
		shell.setText("\u0421\u043E\u0437\u0434\u0430\u043D\u0438\u0435 \u041B\u041A\u0426");
		
		Composite composite = new Composite(shell, SWT.BORDER);
		composite.setBounds(27, 45, 376, 238);
		
		check1 = new Button(composite, SWT.CHECK);
		check1.setSelection(true);
		check1.setBounds(10, 156, 32, 16);
		
		Label label = new Label(composite, SWT.NONE);
		label.setBounds(57, 158, 231, 13);
		label.setText("\u0421\u043E\u0437\u0434\u0430\u0442\u044C \u041B\u041A\u0426");
		
		radioButton1 = new Button(composite, SWT.RADIO);
		radioButton1.setBounds(10, 10, 189, 16);
		radioButton1.setText("\u0438\u0437 \u043F\u0430\u043F\u043A\u0438 , \u0431\u0435\u0437 \u043F\u043E\u0442\u043E\u043C\u043A\u043E\u0432");
		
		radioButton2 = new Button(composite, SWT.RADIO);
		radioButton2.setBounds(10, 44, 139, 16);
		radioButton2.setText("\u0421\u0415 \u0441\u043E \u0441\u0442\u0440\u0443\u043A\u0442\u0443\u0440\u043E\u0439");
		
		radioButton3 = new Button(composite, SWT.RADIO);
		radioButton3.setBounds(10, 79, 329, 16);
		radioButton3.setText("\u043F\u043E \u0432\u044B\u0431\u0440\u0430\u043D\u043D\u043E\u043C\u0443 \u0414\u0414 (\u0441\u0442\u0430\u0440\u0448\u0438\u0435 \u0440\u0435\u0432\u0438\u0437\u0438\u0438 \u0438\u0437\u0434\u0435\u043B\u0438\u0439)");
		
		tfCodeIzd = new Text(composite, SWT.BORDER);
		tfCodeIzd.setBounds(52, 113, 162, 19);
		
		Label lblNewLabel = new Label(composite, SWT.NONE);
		lblNewLabel.setBounds(10, 116, 32, 13);
		lblNewLabel.setText("\u0421\u0415");
		
		Label lblNewLabel_1 = new Label(composite, SWT.NONE);
		lblNewLabel_1.setBounds(224, 116, 23, 13);
		lblNewLabel_1.setText("/");
		
		tfCodeRevIzd = new Text(composite, SWT.BORDER);
		tfCodeRevIzd.setBounds(263, 113, 44, 19);
		
		check2 = new Button(composite, SWT.CHECK);
		check2.setSelection(true);
		check2.setBounds(10, 196, 23, 16);
		
		Label lblNewLabel_4 = new Label(composite, SWT.NONE);
		lblNewLabel_4.setBounds(57, 196, 92, 13);
		lblNewLabel_4.setText("\u041F\u0440\u043E\u0432\u0435\u0440\u044F\u0442\u044C \u0441\u0442\u0430\u0442\u0443\u0441");
		
		btn1 = new Button(shell, SWT.NONE);    //   ********************** btn1
		btn1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				button1 ();
			}
		});
		btn1.setBounds(46, 491, 68, 23);
		btn1.setText("OK");
		
		btn2 = new Button(shell, SWT.NONE);    //  ********************  btn2
		btn2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				button2 ();
			}
		});
		btn2.setBounds(196, 491, 68, 23);
		btn2.setText("Cancel");
		
		comboRR = new Combo(shell, SWT.NONE);
		comboRR.setBounds(157, 321, 139, 21);
		comboRR.setItems(dud.allRR);
		comboRR.setText(dud.allRR[0]);
		
		Label label_1 = new Label(shell, SWT.NONE);
		label_1.setBounds(27, 324, 124, 13);
		label_1.setText("\u043F\u0440\u0430\u0432\u0438\u043B\u043E \u043C\u043E\u0434\u0438\u0444\u0438\u043A\u0430\u0446\u0438\u0438");
		
		Label lblNewLabel_2 = new Label(shell, SWT.NONE);
		lblNewLabel_2.setBounds(27, 369, 87, 13);
		lblNewLabel_2.setText("\u0420\u0430\u0437\u0440\u0430\u0431\u043E\u0442\u0447\u0438\u043A");
		
		tfRazrab = new Text(shell, SWT.BORDER);
		tfRazrab.setBounds(157, 369, 139, 19);
		
		tfIzdelieMC = new Text(shell, SWT.BORDER);
		tfIzdelieMC.setBounds(157, 417, 139, 19);
		
		Label label_2 = new Label(shell, SWT.NONE);
		label_2.setBounds(30, 417, 49, 13);
		label_2.setText("\u0418\u0437\u0434\u0435\u043B\u0438\u0435");
		
		Label label_3 = new Label(shell, SWT.NONE);
		label_3.setBounds(30, 452, 49, 13);
		label_3.setText("\u2116  \u041B\u041A\u0426");
		
		tfLkcIzdNum = new Text(shell, SWT.BORDER);
		tfLkcIzdNum.setBounds(157, 452, 139, 19);
		
		Label lblNewLabel_3 = new Label(shell, SWT.NONE);
		lblNewLabel_3.setBounds(302, 455, 19, 13);
		lblNewLabel_3.setText("/");
		
		tfLkcRevIzdNum = new Text(shell, SWT.BORDER);
		tfLkcRevIzdNum.setText("001");
		tfLkcRevIzdNum.setBounds(327, 449, 49, 19);
	}  // proc 
	  // *********************************
	void button1 () {
		 if (radioButton1.getSelection()) dud.SELECT_CHANGE = 0;
		 if (radioButton2.getSelection()) dud.SELECT_CHANGE = 1;
		 if (radioButton3.getSelection()) dud.SELECT_CHANGE = 2;
		 
		dud.topIzdItemRevId = tfCodeIzd.getText() + "/" + tfCodeRevIzd.getText();
	    dud.numberyl = tfLkcIzdNum.getText() + "/" + tfLkcRevIzdNum.getText();
	    dud.vypolnil = tfRazrab.getText();
	    dud.izdelie = tfIzdelieMC.getText();
	    dud.IS_CREATE_IZD = check1.getSelection();
	    dud.checkStatus = check2.getSelection();
	    int ind = comboRR.getSelectionIndex();
	    if (ind == -1) ind =0;
	    dud.rrSelected = dud.allRR [ind];
	    dud.result = true;
		shell.close();
		shell.dispose();
	}
	  // *********************************
	void button2 () {
		dud.result = false;
		shell.close();
		shell.dispose();
	}
}   // class
