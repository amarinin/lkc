package lkc;

import java.io.File;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author KMuhov
 */
public class MyContentHandler {

    private DocumentBuilderFactory factory = null;
    private DocumentBuilder builder = null;
    private Document document = null;

    private HashMap hmi = new HashMap();
    private HashMap hmd = new HashMap();
    private String module = new String("");

    public MyContentHandler(String path, String module, HashMap hmi, HashMap hmd) {
        try {
            File file = new File(path+"/modules/config.xml");
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            document = builder.parse(file);
            document.getDocumentElement().normalize();

            this.module = module;
            this.hmi = hmi;
            this.hmd = hmd;
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    public Node getModuleConfig() {
        NodeList node_list = document.getElementsByTagName("module");
        for (int i = 0; i < node_list.getLength(); i++) {
            Node mnode = node_list.item(i);
            if (mnode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) mnode;
                if (element.getAttribute("name").equalsIgnoreCase(this.module))
                    return mnode;
            }
        }
        return null;
    }

    public void getHashMaps(Node node) {
        if (node == null) return;   // >>>>>>>>>>>

        if (node.hasChildNodes()) {
            NodeList children = node.getChildNodes();
                     // VVVVVVVVVVVVVVVVVVVV
            for (int i = 0; i < children.getLength(); i++) {
                if (children.item(i).getNodeType() != Node.ELEMENT_NODE) continue;
                if (!children.item(i).hasChildNodes()) continue;

                NodeList nl = children.item(i).getChildNodes();
                            // ooooooooooooooooooo
                for (int j = 0; j < nl.getLength(); j++) {
                    Node _node_ = nl.item(j);
                    if (_node_.getNodeType() != Node.ELEMENT_NODE) continue;

                    Element element = (Element) _node_;
                    if (_node_.getNodeName().equals("item")) {
                        hmi.put(element.getAttribute("code"), element.getAttribute("short"));  // .21(type) - ����
                       // System.out.println("getHashMaps  ----   code / short =" +
                        	//	element.getAttribute("code") + "/" + element.getAttribute("short"));
                    } else if (_node_.getNodeName().equals("dataset")) {
                        NData nd = new NData();
                        nd.id = element.getAttribute("id");
                        nd.yl = element.getAttribute("typendyl");
                        nd.exts = element.getAttribute("exts");
                        hmd.put(element.getAttribute("typend"), nd);  // Text (key) / ��������� �����. ,  ����
                    }
                }        // for // oooooooooooooo
            }    // for // AAAAAAAAAAAAAAAAAA
        }
    }
}

class NData {
    public String id = new String();
    public String yl = new String();
    public String exts = new String();
}