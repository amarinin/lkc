package lkc;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
     // CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC class
public class DialogValid extends Dialog {

	protected Object result;
	protected Shell shell;
	DataValidDlg vd = null;
	Button btn1 = null;
	Button btn2 = null;
	Button btn3 = null;

	Button rb1 = null;
	Button rb2 = null;
	
	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	  // ********************************************* K K K
	public DialogValid (Shell parent, int style, DataValidDlg theVd) {
		super(parent, style);
		setText("SWT Dialog");
		vd = theVd;
	    open (); 
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	  // ***************************************
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	   // *************************************
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(450, 369);
		shell.setText("\u0417\u0430\u0434\u0430\u0442\u044C \u043D\u0430\u0431\u043E\u0440\u044B \u0434\u0430\u043D\u043D\u044B\u0445 \u0434\u043B\u044F \u043E\u0431\u0440\u0430\u0431\u043E\u0442\u043A\u0438");
		
		Composite composite = new Composite(shell, SWT.BORDER);
		composite.setBounds(36, 69, 300, 198);
		
		Label label = new Label(composite, SWT.NONE);
		label.setBounds(38, 0, 231, 13);
		label.setText("\u0412\u044B\u0431\u043E\u0440 \u0432\u0445\u043E\u0434\u043D\u044B\u0445 \u0434\u0430\u043D\u043D\u044B\u0445");
		
		rb1 = new Button(composite, SWT.RADIO);
		rb1.setBounds(21, 35, 211, 16);
		rb1.setText("\u0418\u0437 \u0432\u044B\u0431\u0440\u0430\u043D\u043D\u043E\u0433\u043E \u043D\u0430\u0431\u043E\u0440\u0430 \u0434\u0430\u043D\u043D\u044B\u0445");
		
		rb2 = new Button(composite, SWT.RADIO);
		rb2.setBounds(21, 80, 141, 16);
		rb2.setText("\u0418\u0437 xml-\u0444\u0430\u0439\u043B\u0430");
		
		btn1 = new Button(shell, SWT.NONE);    //   ********************** btn1
		btn1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				button1 ();
			}
		});
		btn1.setBounds(96, 293, 68, 23);
		btn1.setText("OK");
		
		btn2 = new Button(shell, SWT.NONE);    // ************************  btn2
		btn2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				button2 ();
			}
		});
		btn2.setBounds(237, 293, 68, 23);
		btn2.setText("Cancel");

	}  // proc 
	  // *********************************
	void button1 () {
        if (rb1.getSelection()) vd.SELECT_CHANGE = 0;
        if (rb2.getSelection()) vd.SELECT_CHANGE = 1;
		vd.result = true;
		shell.close();
		shell.dispose();
	}
	  // *********************************
	void button2 () {
		vd.result = false;
		shell.close();
		shell.dispose();
	}
}   // class
