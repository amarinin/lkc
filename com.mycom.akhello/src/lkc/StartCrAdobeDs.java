package lkc;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.*;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCComponentRevisionRule;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
//TCComponentDatasetType
import com.teamcenter.rac.kernel.TCComponentDatasetType;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.MessageBox;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.jface.window.*;    // ok
// import com.mycom.akhello.actions.FrameDS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchWindow;

import java.io.Console;

import org.jacorb.poa.gui.poa.POAFrame;

import com.teamcenter.rac.aifrcp.LoggingConsole;

import org.eclipse.osgi.framework.internal.core.*;
//  import org.eclipse.ui.articles.views.*;       // ����� ������������ ��� ������ �� ����� ���
       // CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
public class StartCrAdobeDs {
	// DataTrMta data = new DataTrMta ();
	// FrameLoadGiz frame = null;          
	TCSession session = null;
	AIFComponentContext context = null;
	Object [] obs = null;
	DataULdlg dud = new DataULdlg ();
	Shell shell = null;           
	IWorkbenchWindow window = null;
	
	String filePDF = new String ("C:\\rpc\\compLkc\\wfiles\\d_buffer.pdf");
	
	         // *********************************************       K    K     K
public	StartCrAdobeDs (TCSession theSession, AIFComponentContext theContext,
		Object [] theObs, IWorkbenchWindow theWindow )	{                                    
		session = theSession;
		context = theContext;
		obs = theObs;
		window = theWindow;
		      // place = this;
			 System.out.println(" StartLoadMta  ---------- constructor ----- start ");
	        start ();   // >>>>>>>>>>>>>>>
  	}    // proc ------ K K K
  // ************************************************
void start ()  {
	          System.out.println(" Start.. ---------- start ");
	                        session.setStatus("load starts");
	  	AIFComponentContext context = (AIFComponentContext)obs [0];
	  	
	  	TCComponentItemRevision ir = (TCComponentItemRevision) context.getComponent();          
	      if (!(ir instanceof TCComponentItemRevision)) {
	          MessageBox.post("��������� ������ �� �������� �������� �������. ���������� �����...", "������!", MessageBox.ERROR);
	          return ;
	      } 
	      findCreateLinkDS (ir); 
	      System.out.println(" Start.. ---------- finish ");
}   // proc
    // *********************************************
TCComponentDataset findCreateLinkDS (TCComponentItemRevision ir)
 {                 // �������� ��� ��������� �� ���, ����� �� ������
	TCComponentDataset dataset = null;
      // ����������� ����� ��� ������ � ��
       String[] fdsNames = new String[1];
       fdsNames[0] = filePDF;
       String[] nRefsDs = new String[1];
       nRefsDs[0] = "PDF";       // Adobe - old
       boolean found = false;
           System.out.println("findCreateDS  ------------- start");
       try {
    	  TCComponentItem itW = ir.getItem();
                      // ������ ����� �.�. - ������
          String dsName = new String ("test name");
          dsName = itW.getProperty("item_id").toString()+ "/" +
                 ir.getProperty("item_revision_id").toString() + "_Lkc";

                         // 1 - ������� ����� ������������ �.�.
           AIFComponentContext[] context = ir.getRelated("IMAN_specification");
           if (context != null) {
               for (int i = 0; i < context.length; i++) {
                   String dsType = context[i].getComponent().getProperty("object_type");
                   String dsn = context[i].getComponent().getProperty("object_name");
                   if (dsType.equals("PDF") && dsn.equals(dsName) ) {       // Adobe
                       dataset = (TCComponentDataset) context[i].getComponent();
                       found = true;
                         System.out.println("findCreateDS  ------------- ds found");
                       break;
                   }
               }         //    for
           }
           if (found) {
                 dataset.setFiles(fdsNames, nRefsDs);   // ������������� ��� �����. ������ ���������.
                 return dataset;                            // >>>>>>>>>>>
           }
           else {
                 System.out.println(" createValidDs --------  Point 2 ");
			 TCComponentDatasetType dsType = null;
			 dsType = (TCComponentDatasetType) session.getTypeComponent("Dataset");
			     System.out.println("findCreateDS --------- before creation ds, dsName =" + dsName);
			         //  ------------ create ds
			 dataset = dsType.create(dsName, "", "PDF");     // Adobe
			 dataset.setFiles(fdsNames, nRefsDs);   // ������������� ��� �����. ������ ���������.      	   
           }
                     // ------- ����� ����� ��� ....
                       // ���������� �� � �������
           Vector vector = new Vector();
           vector.addElement(dataset);
           ((TCComponent) ir).insertRelated("IMAN_specification", vector, 0);
               System.out.println("findCreateDS ---------finished" );
       } catch (Exception ex) {  ex.printStackTrace(); }
  return dataset;
 }
    // **********************************************
}         // class