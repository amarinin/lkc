package lkc;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileInputStream;

/**
 * ����� �������� ����������� �����
 * @author KMuhov
 */
public class ControlSummary {

    private long get_bytes = 0;
    private long number_bytes = 0;
    private long MAX_VALUE = Long.parseLong("FFFFFFFF", 16);
    private long ESI_VALUE = Long.parseLong("00000777", 16);
    private FileInputStream inStream;

    /**
     * ����������� ��������� ����������� �����
     * @param path_file ��� ��������������� �����
     */
    public ControlSummary(String path_file) {
        try {
            File inFile = new File(path_file);
            inStream = new FileInputStream(inFile);
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    public ControlSummary() { }

    /**
     * ������� �������� ���������� ���� � �����
     */
    public void getBytesFromFile() {
        try {
            int c = 0;
            while((c = inStream.read()) != -1) {
                get_bytes += c;
                /* ����������� ���������� ����� � �����: ��� ������������
                   ����� ���������� � ����������� ������� � �������� �������
                   ���������� ����������� ��������� MAX_VALUE */
                if (get_bytes > MAX_VALUE) get_bytes = get_bytes - MAX_VALUE;
                number_bytes += c;
            }
            inStream.close();
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    /**
     * ������� ���������� ��������� �������� ����������� ������
     * @return
     */
    public String getSummaryBytes() {
        return String.valueOf(get_bytes);
    }

    /**
     * ������� ���������� ������ ����� � ������
     * @param path ��� ����� � ����� ��� ������������
     * @return
     */
    public String getBytesFile(String path) {
        File file = new File(path);
        return String.valueOf(file.length());
    }

    /**
     * ������� ���������� ���������� ����
     * @return
     */
    public long getNumberBytes() { return this.number_bytes; }

    /**
     * �������������� � ��������� 16-������ ���
     * @return ����������������� �������� ������� �����
     */
    public String getHexData() {
        String hex_string = Long.toHexString(get_bytes);
        if (hex_string.length() < 8) 
            while (hex_string.length() < 8) hex_string = "0" + hex_string;
        return hex_string;
    }

    public String getHexData(long csum) {
        String hex_string = Long.toHexString(csum);
        if (hex_string.length() < 8)
            while (hex_string.length() < 8) hex_string = "0" + hex_string;
        return hex_string;
    }

    /**
     * ������� �������� ����������� ����� ��� ���:<br/>
     * � ����������� ����� ��������� ����� ����������� ����������������� ����� 0�00000777
     * @param csum ����������� ����� ��������� �����
     * @return ����������� ����� ��� ���
     */
    public long getConcatSum(long csum) {
        csum += ESI_VALUE;
        if (csum > MAX_VALUE) csum = csum - MAX_VALUE;
        return csum;
    }
}