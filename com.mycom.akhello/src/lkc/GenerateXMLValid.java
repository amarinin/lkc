package lkc;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author KMuhov
 */
public class GenerateXMLValid {

    private String path;
	private DocumentBuilder builder = null;
    private Document document = null;
    private Element root = null;

    public HashMap hash_data = new HashMap();
    public String numberyl = new String("");
    public boolean PRINT_VALID = false;
    public String generate_date_now = new String("");
    public String rrCur = new String("");  
    
    public GenerateXMLValid(String path){
    	this.path = path;
    }

    private void initParams() {
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = builder.newDocument();
            root = document.createElement("IPCPublication");
            root.setAttribute("Description", "������");
            root.setAttribute("PublicationCode", generate_date_now);
            root.setAttribute("Title", numberyl);
            root.setAttribute("RRcurrent", rrCur);         
            document.appendChild(root);
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    private void createData() {
        if (PRINT_VALID)
            System.out.println("\nPrint validation...");
        Iterator iterator = hash_data.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next().toString();
            DataRoot dr = (DataRoot) hash_data.get(key);
            Element parent = createItem(dr);
            for (int i = 0; i < dr.da.length; i++)
                createDs(parent, dr.da[i]);
        }    // while
    }

    private Element createItem(DataRoot dr) {
        Element item = document.createElement("Item");
        if (PRINT_VALID)
            System.out.println("    valid item = " + dr.id + "/" + dr.ver + " - " + dr.object_type);
        item.setAttribute("id", dr.id);
        item.setAttribute("name", dr.name);
        item.setAttribute("type_de", dr.type);
        item.setAttribute("version", dr.ver);
        item.setAttribute("type", dr.object_type);
        root.appendChild(item);
        return item;
    }

    private void createDs(Element parent, DataAttr da) {
        Element ds = document.createElement("Ds");
        ds.setAttribute("obozn", da.id);
        ds.setAttribute("name", da.name);
        ds.setAttribute("type", da.tn);
        ds.setAttribute("ksum", da.csum);
        parent.appendChild(ds);
    }

    private boolean writeXmlFile(String numberyl) {
        try {
            File file = new File(path + "/validation/"+getCleanString(numberyl)+"_valid.xml");
            Source source = new DOMSource(document);
            Result result = new StreamResult(file);
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty(OutputKeys.ENCODING, "windows-1251");
            xformer.setOutputProperty(OutputKeys.INDENT, "yes");
            xformer.transform(source, result);
            return true;
        } catch (Exception ex) { ex.printStackTrace(); return false; }
    }

    public boolean createXML(String numberyl) {
        initParams();
        createData();
        return writeXmlFile(numberyl);
    }

    private String getCleanString(String str) {
        return str.replaceAll("/", "-");
    }
}
