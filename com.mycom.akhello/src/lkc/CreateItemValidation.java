package lkc;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.lov.LOVPopupButton;
import com.teamcenter.rac.kernel.TCClassService;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentReleaseStatus;
import com.teamcenter.rac.kernel.TCComponentReleaseStatusType;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCFormProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCTypeService;
import com.teamcenter.rac.stylesheet.AbstractRendering;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.Separator;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.aif.kernel.AIFSessionManager;
import com.teamcenter.rac.aif.kernel.AIFSession;
import com.teamcenter.rac.tcapps.TcappsPlugin;
import com.teamcenter.rac.aifrcp.RCPPortal;
 // import com.teamcenter.rac.aif.kernel.AbstractAIFComponent;
import com.teamcenter.rac.aif.kernel.ITCSession;
import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.*;

import com.teamcenter.rac.aif.*;
import com.teamcenter.rac.aif.kernel.*;
import com.teamcenter.rac.aifrcp.*;
import com.teamcenter.rac.util.*;
import com.teamcenter.rac.*;
import com.teamcenter.rac.operationaldata.*;
import org.eclipse.jface.viewers.ISelection;

import com.teamcenter.rac.kernel.TCComponentDatasetType;
import java.util.*;
import java.io.*;
import javax.swing.*;
public class CreateItemValidation {
    private TCComponentItem itemGl;
    private TCComponentItemRevision itemRevGl;
    private String itemValidIdGl;
    private String itemRevValidIdGl;
    private boolean PRINT;
        // ****************************************
    public TCComponentItem get_itemGl() { return itemGl; }

    public TCComponentItemRevision get_itemRevGl() { return itemRevGl; }

    public String get_idGl() { return itemValidIdGl; }

    public String get_revIdGl() { return itemRevValidIdGl; }

        // *****************************************    K   K   K
    public CreateItemValidation(boolean PRINT) {
      itemGl = null;
      itemRevGl = null;
      itemValidIdGl = "";
      itemRevValidIdGl = "";
      this.PRINT = PRINT;
    }
    public int createValidMain(TCSession session, String ulNum) {
        // --------- ������� ��������
        //   1. ������ (� ������� ���������) -- > ����� �� �� ������ ���������   777_v1/v2.....
        //   ����� �� - 47601.0201.000.000.70/V1 - �� ����� ��������� � �������� ������
        //   �������-����� - 70  ����� ������� ���������� � �������� ��
        //   ������� �������-������ - �� - ����, �� - ���
        //   � ����� �� - ���� ����
        //   �� ������� - 47601.0201.000.000.70
        //   ��� �� - 47601.0201.000.000.70_Audit/V1

        //     2. � ������ ������ �� ��������� ������� ������ �� � ������ ������ ���������

        //   ��� ���������� �/�  0 - ���������� ���������� �/�
        //                       1-  ������������ ����� ��
        //                       2-  ������ ��� ������ � ��
        //                       3 - ������� �����
        //                       4 - ������� �� �������

        TCComponent itemComp = null;
        TCComponentItem item = null;
        TCComponentItemRevision itemRev = null;
        TCComponentFolder tFolder = null;
        String itemValidId = "";
        String itemRevValidId = "";
        int res = 0;
        boolean f = false;
        String[] ms = null;

        try {
//            service = (IMANUserService) session.getUserService();
        	TCClassService classService = session.getClassService();
              System.out.println("createValidMain --------start,  ulNum (src)  =" + ulNum );
                    //  -- ��������, ��������� ������ ��
            ms = ulNum.split("/", -1);
           if (ms.length != 2  ) {           
                res = 1;
                   if (PRINT) System.out.println("createValidMain --------ulNum - wrong, ulNum =" + ulNum );
                     System.out.println("\tcreate ItemValid wrog!");
                MessageBox.post("�������� ����� ���. ������� \"���\" �� ����� �������.", "��������� � ����������", MessageBox.ERROR);
                return res;    // >>>>
            }
             if (PRINT) System.out.println(">>>>>> createValidMain -------- ulNum (after checking)  =" + ulNum );
            itemValidId = ms[0];
            itemRevValidId = ms[1];

                        // �����-������� ����� - ��������� �� ��
            tFolder = findCrFolder(session, "������� ���");             // call  >>>

                   // --------------- ����� ���. ���������
            String sStartUser = "";
            TCComponentUser userComp;
            userComp = session.getUser();
            TCComponentItemType itemTypeComp = null;
            itemTypeComp = (TCComponentItemType) session.getTypeComponent("Item");
            itemComp = (TCComponent) itemTypeComp.find(itemValidId);

            if (itemComp == null) {   // -------- ������� ��� ���   --- ������
                       // �������� ���.(� �������) � ������� � �����.
            	TCComponent[] iComps = createItemValid(session, itemValidId, itemRevValidId, tFolder);
                if (iComps == null || iComps.length != 2 || iComps[0] ==null || iComps[1] == null) {
                    res = 4;
                    return res;
                }
                item = (TCComponentItem) iComps[0];
                itemRev = (TCComponentItemRevision) iComps[1];
                f = createValidDs(session, itemRev, itemValidId, itemRevValidId);  // >>>>>>
                if (!f) {
                    res = 2;
                    return res;
                }
                else        // ���������, ��������� ���������� ����������
                {
                  if (item == null)   System.out.println("createValidMain ---------item = null" );
                  if (itemRev == null)   System.out.println("createValidMain ---------itemRev = null" );
                  itemGl = item;
                  itemRevGl = itemRev;
                  itemValidIdGl = itemValidId;
                  itemRevValidIdGl = itemRevValidId;
                    //System.out.println("createValidMain ---------set global, point 1" );
                }

            }    // -------- ������� ��� ���   --- �����
            else // ������� ����������, �����-�������� �������   --- ������
            {
                  // �������� ���������
                sStartUser = userComp.getProperty(TCComponentUser.PROP_USER_ID.toString());
                    //System.out.println("createValidMain ----------- sStartUser =" + sStartUser );
                if (!SpcUtils.isStartUser (itemComp, sStartUser)) {
                  res = 3;
                  return res;
                }
                        //  -------  ����� ������� ���. ���������
                item = (TCComponentItem) itemComp;
                TCComponentItemRevisionType itemRevType = null;
                itemRevType = (TCComponentItemRevisionType) session.getTypeComponent("ItemRevision");
                itemRev = itemRevType.findRevision(itemValidId, itemRevValidId);

                if (itemRev == null) {   // ������� �� �������, �������� ��
                              // �������� �������
                    itemRev = item.getLatestItemRevision();    // ������ ����� ������� � ���������� �� ...
                    itemRev = item.revise(itemRevValidId, "���� �������� �����������", "");
                    // ������ ���� ������ ���� ������ �������. ��� saveAs - �������� ��� � ����������
                }
                else {       // ������� ����. ��������� , �����.
                    res = 2;
                    return res;
                }
                f = createValidDs(session, itemRev, itemValidId, itemRevValidId);
                if (!f) {
                    res = 2;
                    return res;
                }
                else        // ���������, ��������� ���������� ����������
                {
                  if (item == null)   System.out.println("createValidMain ---------item = null" );
                  if (itemRev == null)   System.out.println("createValidMain ---------itemRev = null" );
                  itemGl = item;
                  itemRevGl = itemRev;
                  itemValidIdGl = itemValidId;
                  itemRevValidIdGl = itemRevValidId;
                     // System.out.println("createValidMain ---------set global, point 2" );
                }
            }       // ������� ����������, �����-�������� ������� --------- �����
        } catch (Exception e) {
            System.out.println(" try - error ********** ");
            e.printStackTrace();
            return res;
        }
                    // ------  window and bom top line  --------------------------

        return res;
    }   // proc
    // *****************************************************
     public static boolean createValidDs(TCSession session, TCComponentItemRevision itemRev, String itemId, String itemRevId) {
        // �����-������� ��-����� ��� ������� � �������� ���� ����
        boolean res = false;
        String dsName = "";
        TCComponentDataset dataset = null;
           System.out.println(" createValidDs ------------- start");
          // ����� ���� � validation
        String complSpec = "";
        complSpec = "c:\\rpc\\compLkc\\validation\\" + itemId + "-" + itemRevId + "_valid.xml";
        File f = new File(complSpec);
        // See if it actually exists
        if (!f.exists()) {
            System.out.println("createValidDs *************  file not found in validation folder");
            return res;
        }
        //   �������� ��� ��
        dsName = itemId + "_Audit/" + itemRevId;
        // ����������� ����� ��� ������ � ��
        String[] fdsNames = new String[1];
        fdsNames[0] = complSpec;
        String[] nRefsDs = new String[1];
        nRefsDs[0] = "XML";

        //   ����� �� ��� ��������
        boolean found = false;
        try {
            AIFComponentContext[] context = itemRev.getRelated("IMAN_specification");
            if (context != null) {
                for (int i = 0; i < context.length; i++) {
                    String dsType = context[i].getComponent().getProperty("object_type");
                    String dsNameCur = context[i].getComponent().getProperty("object_name");
                    if (dsNameCur.equals(dsName)) {
                        dataset = (TCComponentDataset) context[i].getComponent();
                        found = true;
                        break;
                    }
                }         //    for
            }
                System.out.println(" createValidDs ----------- ds found flag = " + found);
            if (found) {
                dataset.removeFiles("XML");
                dataset.setFiles(fdsNames, nRefsDs);   // ������������� ��� �����. ������ ���������.
                res = true;
                return res;                      // >>>>>>>>>>>
            }

                       // ------- ����� ����� ��� ������������� �� � ��������....
                   System.out.println(" createValidDs -------- for absent dss and ..... ");
            TCComponentDatasetType dsType = null;
            //IMANComponent dsComp = null;
            TCComponent[] dsComps;

            dsType = (TCComponentDatasetType) session.getTypeComponent("Dataset");
            dsComps = dsType.findAll(dsName);
//            if (dsComps != null) {
//                // ��������������� ����������� ����� ������� ������
//                for (int i = 0; i < dsComps.length; i++) {
//                //System.out.println(" createValidDs --------  before delete ");
//                    IMANComponent dsComp = dsComps[i];
//                    if (isStartUser(dsComp, session.getUser().getUserId()) && !isReservation(dsComp, session)) {
//                        System.out.println("before remove");
//                        itemRev.remove("IMAN_specification", dsComp);
//                        System.out.println("after remove");
//                        dsComp.delete();
//                        System.out.println("after delete");
//                    } else {
//                        MessageBox.post("�� ���� ������� ����� ������ ��, name=" + dsName, "Error", MessageBox.ERROR);
//                    }
//                }
//            }

                   // ����� ������ �����������. �������� � ����
                       //  ------------ create ds
                   System.out.println(" createValidDs ----------- before create ds ");
            dataset = dsType.create(dsName, "", "XMLAuditLog");
            dataset.setFiles(fdsNames, nRefsDs);   // ������������� ��� �����. ������ ���������.
            // ���������� �� � �������
            Vector vector = new Vector();
            vector.addElement(dataset);
            ((TCComponent) itemRev).insertRelated("IMAN_specification", vector, 0);
            res = true;
                     System.out.println(" createValidDs ============== finished ");
        } catch (Exception ex) { ex.printStackTrace(); }
        return res;
    }
     // **************************************************
    public static String[] getULversionZapr(String ulNum) {
            // ��� ����� ������������� ���������� ��� ��-�� ���. ������� - ������ ������
            // ���� ���������, �������� 0 - item Id , 1 - version Id
        String[] res = new String[2];
        String[] ms = null;
            System.out.println(" getULversionZapr --------- start----ulNum ="+ ulNum);
        ms = ulNum.split("/", -1);
        if (ms.length != 2 ) { 
            res[0] = "";
            res[1] = "";
            return res;
        }
        res[0] = ms[0];
        res[1] = ms[1];
        return res;
    }
    // *****************************************************
    public static TCComponentItemRevision itemVer4zaprExists(TCSession session, 
    		 String itemValidId, String itemRevValidId) {     // ������, �� ���������� 
    	
        TCComponentItemRevision itemRev = null;
        try {
                           // ����� ������� ������� ���������
            TCComponentItemRevisionType itemRevType = null;
            itemRevType = (TCComponentItemRevisionType) session.getTypeComponent("ItemRevision");
            itemRev = itemRevType.findRevision(itemValidId, itemRevValidId);
            if (itemRev == null) {
                /*System.out.println(" itemVer4zaprExists ------  item Revision not found,  "
                        + itemValidId + "/" + itemRevValidId);*/
            }
        } catch (Exception e) {         e.printStackTrace();
            System.out.println(" try - error ********** ");
          return itemRev;
        }
        return itemRev;
    }
    // *********************************************************************
    public TCComponent[] createItemValid(TCSession session, String itemId,
                             String itemRevId, TCComponentFolder folder) // in:
    // out: newItem, itemRevision,   (tags)
    //       ������ �� ����.
    //  ��������� ������� ���� � �����
    {
        int i = -1;
        TCComponentItemType itemTypeComponent = null;
        TCComponentItem newItem;
        TCComponentItemRevision itemRevision;
        TCComponent[] iComps = null;

           System.out.println("createItem -----------start");
        try {
            itemTypeComponent = null;
            itemTypeComponent = (TCComponentItemType) session.getTypeComponent("Item");
            newItem = itemTypeComponent.create(itemId, itemRevId, "I8_LKC", "���", "", null);
             System.out.println(" createItem -------- item created, item id/rev = " + itemId + " / " + itemRevId);  // if (PRINT)

            itemRevision = newItem.getLatestItemRevision();
            iComps = new TCComponent[2];
            iComps[0] = newItem;
            iComps[1] = itemRevision;

                // ��������� newItem � ������� �����
            Vector vector = new Vector();
            vector.addElement(newItem);
            folder.insertRelated("contents", vector, 0);
                        System.out.println("createItem ----------- - createItem - finish");
        } catch (TCException e) {  e.printStackTrace();
            System.out.println(" createItem  ************ exception ");
        }
        return iComps;
    }  // proc 
  //********************************************************
 // main
     public static TCComponentFolder findCrFolder(TCSession session, String folderName) //     ------------      ������ ������� ����� � ,���� ���, �������
     // in:   folderName
     // out:  folder tag
     //  w :     tFolder
     {
    	 TCComponentFolder tFolder = null;
         try {
             // C������� ����� � ������ ������� � �����
                           // ����� ����� home
                     //System.out.println(" findFolder - start working with  folder Home ");
        	 TCComponent userComp;
             userComp = session.getUser();
             TCComponentFolder home = ((TCComponentUser) userComp).getHomeFolder();
             TCComponent[] folderComps;                 // ����� ��� ������
             TCComponentFolderType folderType;
             tFolder = null;                      // �����, ������� ����

             // ������� ������� ����� � ��������� ���� item
             folderComps = home.getRelatedComponents("contents");
                         //System.out.println(" findFolder - start looking for folder1 ");
                                // ���� ������ �����
             boolean folder_found = false;
             for (int i = 0; i < folderComps.length; i++) {
                 if (folderComps[i].getTCProperty("object_name").getStringValue().equals(folderName)) {
                     //System.out.println("  - findFolder -  folder  found - break ");
                     tFolder = (TCComponentFolder) folderComps[i];
                     folder_found = true;
                     break;
                 }
             }
             if (!folder_found) // ���� ����� ��� , �� �� �� ��������
             {
                   //System.out.println("  - findFolder -  folder not found, let create it ");
                 folderType = (TCComponentFolderType) session.getTypeComponent("Folder");
                 tFolder = folderType.create(folderName, "folder descriptioin", "Folder");
                 if (tFolder == null) {
                     System.out.println("findFolder - empty the folder tag ");
                 } else {
                     System.out.println("findFolder - inserting non-null folder into home ");
                     Vector vector3 = new Vector();
                     vector3.addElement(tFolder);
                     ((TCComponent) home).insertRelated("contents", vector3, 0);
                 }
             }
         } catch (TCException e) {  e.printStackTrace();
                  System.out.println(" - findFolder - IMANException -creating/finding folder ");
          }                      // catch
         return tFolder;
     }  // proc  
    // *****************************************************
}      // class ---   CreateItemValidation
