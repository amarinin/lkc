package lkc;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.zip.CRC32;

/**
 * ����� �������� ����������� �����
 * @author KMuhov
 * @author AMarinin
 */
public class ControlSummaryCRC32 {

    private long get_bytes = 0;
    private long number_bytes = 0;
    private long MAX_VALUE = Long.parseLong("FFFFFFFF", 16);
    private long ESI_VALUE = Long.parseLong("00000777", 16);
    //private FileInputStream inStream;
    private BufferedInputStream inStream;
    private CRC32 crc32 = new CRC32();

    /**
     * ����������� ��������� ����������� �����
     * @param path_file ��� ��������������� �����
     */
    public ControlSummaryCRC32(String path_file) {
        try {
            File inFile = new File(path_file);
            //inStream = new FileInputStream(inFile);
            inStream = new BufferedInputStream(new FileInputStream(inFile));
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    public ControlSummaryCRC32() { }
    
    //main ������ ��� �����������
    public static void main(String[] args){
    	String path = "/home/amarinin/distr/nx-8.0.3.mp04.tar.gz";
    	System.out.println("Start");
    	ControlSummaryCRC32 s = new ControlSummaryCRC32(path);
    	s.getBytesFromFileCRC32();
    	System.out.println("Stop");
    	System.out.println(s.getHexData());
    	
    }

    /**
     * ������� �������� ���������� ���� � �����
     */
    public void getBytesFromFile() {
    	int c = 0;
    	get_bytes = 0;
    	number_bytes = 0;
        try {            
            while((c = inStream.read()) != -1) {
                get_bytes += c;
                /* ����������� ���������� ����� � �����: ��� ������������
                   ����� ���������� � ����������� ������� � �������� �������
                   ���������� ����������� ��������� MAX_VALUE */
                if (get_bytes > MAX_VALUE) get_bytes = get_bytes - MAX_VALUE;
                number_bytes ++;
            }
            inStream.close();
        } catch (Exception ex) { ex.printStackTrace(); }
    }
        
    /**
     * ������� ��������� �������� ���������� ����� ��������� ����������� �������� 
     * CRC32 ��������� � ������� JDK
     * ������ �������� �� �����
     */
    public void getBytesFromFileCRC32(){
    	int c = 0;
    	get_bytes = 0;
    	crc32.reset();
        try {            
            while((c = inStream.read()) != -1) {
                crc32.update(c);
            }
            inStream.close();
            get_bytes = crc32.getValue();
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    /**
     * ������� ���������� ��������� �������� ����������� ������
     * @return
     */
    public String getSummaryBytes() {
        return String.valueOf(get_bytes);
    }

    /**
     * ������� ���������� ������ ����� � ������
     * @param path ��� ����� � ����� ��� ������������
     * @return
     */
    public String getBytesFile(String path) {
        File file = new File(path);
        return String.valueOf(file.length());
    }

    /**
     * ������� ���������� ���������� ����
     * @return
     */
    public long getNumberBytes() { return this.number_bytes; }

    /**
     * �������������� � ��������� 16-������ ���
     * @return ����������������� �������� ������� �����
     */
    public String getHexData() {
        String hex_string = Long.toHexString(get_bytes);
        if (hex_string.length() < 8) 
            while (hex_string.length() < 8) hex_string = "0" + hex_string;
        return hex_string;
    }

    public String getHexData(long csum) {
        String hex_string = Long.toHexString(csum);
        if (hex_string.length() < 8)
            while (hex_string.length() < 8) hex_string = "0" + hex_string;
        return hex_string;
    }

    /**
     * ������� �������� ����������� ����� ��� ���:<br/>
     * � ����������� ����� ��������� ����� ����������� ����������������� ����� 0�00000777
     * @param csum ����������� ����� ��������� �����
     * @return ����������� ����� ��� ���
     */
    public long getConcatSum(long csum) {
        csum += ESI_VALUE;
        if (csum > MAX_VALUE) csum = csum - MAX_VALUE;
        return csum;
    }
}
