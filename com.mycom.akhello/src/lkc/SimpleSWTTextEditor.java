package lkc;

import java.io.*;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

public class SimpleSWTTextEditor {

	private org.eclipse.swt.widgets.Shell sShell = null;  //  @jve:decl-index=0:visual-constraint="133,13"

	private Button button2 = null;

	private boolean hasChanged = false;
	private boolean isClosing = false;

	private static final String title = "Simple Text Editor";

	private static final String NEW_LINE = System.getProperty("line.separator");

	private Combo combo = null;

	private Text tfRazrab = null;

	private Composite composite1 = null;

	private Text tfIzdelieMC = null;

	private Text tfLkcIzdNum = null;

	private Button button1 = null;

	private Label label4 = null;

	private Label label5 = null;

	private Label label6 = null;

	private Label label7 = null;

	private Text tfLkcRevIzdNum = null;

	private Label label14 = null;
	
	DataULdlg dud = new DataULdlg ();  //  @jve:decl-index=0:

	private Label label2 = null;

	private Text tfCodeIzd = null;

	private Label label3 = null;

	private Text tfCodeRevIzd = null;

	private Button radioButton3 = null;

	private Label label12 = null;

	private Button radioButton2 = null;

	private Label label11 = null;

	private Button radioButton1 = null;

	private Label label10 = null;

	private Label label1 = null;
	
	
	
	
	
	// ******************************************************

	/**
	 * This method initializes combo	
	 *
	 */
	private void createCombo() {
		combo = new Combo(sShell, SWT.NONE);
		combo.setBounds(new Rectangle(148, 165, 93, 21));
	}
	
	
	
	
	
	/* Before this is run, be sure to set up the following in the launch configuration 
	 * (Arguments->VM Arguments) for the correct SWT library path. 
	 * The following is a windows example:
	 * -Djava.library.path="installation_directory\plugins\org.eclipse.swt.win32_3.0.0\os\win32\x86"
	 */
	/*
	public static void main(String[] args) {

		org.eclipse.swt.widgets.Display display = org.eclipse.swt.widgets.Display
				.getDefault();
		SimpleSWTTextEditor thisClass = new SimpleSWTTextEditor();
		thisClass.createSShell();
		thisClass.sShell.open();

		while (!thisClass.sShell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
    */
	
	public SimpleSWTTextEditor (DataULdlg theDud) {           // *******************   K    K     K
           // ------------------
		dud = theDud;
		org.eclipse.swt.widgets.Display display = org.eclipse.swt.widgets.Display
				.getDefault();
		//     SimpleSWTTextEditor thisClass = new SimpleSWTTextEditor();
		this.createSShell();
		this.sShell.open();

		while (!this.sShell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		// display.dispose();
	}	
	
	// ************************************************************8
	
	/**
	 * This method initializes sShell
	 */
	private void createSShell() {
		sShell = new org.eclipse.swt.widgets.Shell();
		sShell.setLayout(null);
		createCombo();
		Label filler2 = new Label(sShell, SWT.NONE);
		filler2.setBounds(new Rectangle(99, 3, 0, 13));
		button2 = new Button(sShell, SWT.NONE);
		sShell.setText(title);
		tfRazrab = new Text(sShell, SWT.BORDER);
		tfRazrab.setBounds(new Rectangle(149, 195, 116, 19));
		button2.setText("Exit");
		button2.setBounds(new Rectangle(259, 274, 30, 23));
		createComposite1();
		sShell.setSize(new Point(529, 468));
		tfIzdelieMC = new Text(sShell, SWT.BORDER);
		tfIzdelieMC.setBounds(new Rectangle(151, 223, 115, 19));
		tfLkcIzdNum = new Text(sShell, SWT.BORDER);
		tfLkcIzdNum.setBounds(new Rectangle(151, 248, 116, 19));
		button1 = new Button(sShell, SWT.NONE);
		button1.setBounds(new Rectangle(179, 276, 33, 23));
		button1.setText("OK");
		button1.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				System.out.println("widgetSelected()"); // TODO Auto-generated Event stub widgetSelected()
			}
		});
		
		button1.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				System.out.println("widgetSelected()"); 
				// TODO Auto-generated Event stub widgetSelected()
				doOK ();
			}
		});
		
		label4 = new Label(sShell, SWT.NONE);
		label4.setBounds(new Rectangle(31, 196, 88, 13));
		label4.setText("�����������");
		label5 = new Label(sShell, SWT.NONE);
		label5.setBounds(new Rectangle(31, 224, 65, 13));
		label5.setText("�������");
		label6 = new Label(sShell, SWT.NONE);
		label6.setBounds(new Rectangle(30, 250, 65, 13));
		label6.setText("����� ���");
		label7 = new Label(sShell, SWT.NONE);
		label7.setBounds(new Rectangle(282, 253, 25, 13));
		label7.setText("/");
		tfLkcRevIzdNum = new Text(sShell, SWT.BORDER);
		tfLkcRevIzdNum.setBounds(new Rectangle(315, 247, 76, 19));
		label14 = new Label(sShell, SWT.NONE);
		label14.setBounds(new Rectangle(27, 165, 102, 13));
		label14.setText("������� �����������");
		label2 = new Label(sShell, SWT.NONE);
		label2.setBounds(new Rectangle(28, 145, 109, 13));
		label2.setText("��� ������� �������");
		tfCodeIzd = new Text(sShell, SWT.BORDER);
		tfCodeIzd.setBounds(new Rectangle(149, 138, 76, 19));
		label3 = new Label(sShell, SWT.NONE);
		label3.setBounds(new Rectangle(237, 141, 10, 13));
		label3.setText("/");
		tfCodeRevIzd = new Text(sShell, SWT.BORDER);
		tfCodeRevIzd.setBounds(new Rectangle(261, 137, 76, 19));
		radioButton3 = new Button(sShell, SWT.RADIO);
		radioButton3.setBounds(new Rectangle(27, 102, 13, 16));
		label12 = new Label(sShell, SWT.NONE);
		label12.setBounds(new Rectangle(61, 104, 195, 13));
		label12.setText("�� ��������� ���� �������(��. ����)");
		radioButton2 = new Button(sShell, SWT.RADIO);
		radioButton2.setBounds(new Rectangle(27, 81, 13, 16));
		label11 = new Label(sShell, SWT.NONE);
		label11.setBounds(new Rectangle(60, 83, 197, 13));
		label11.setText("�� ��������� �������� (� ���������)");
		radioButton1 = new Button(sShell, SWT.RADIO);
		radioButton1.setBounds(new Rectangle(26, 62, 13, 16));
		label10 = new Label(sShell, SWT.NONE);
		label10.setBounds(new Rectangle(61, 65, 159, 13));
		label10.setText("������� �� ����� ��� ��������");
		
	
		button2
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						doExit();
					}
				});
		
		sShell.addShellListener(new org.eclipse.swt.events.ShellAdapter() {
			public void shellClosed(org.eclipse.swt.events.ShellEvent e) {
				if (!isClosing) {
					e.doit = doExit();
				
				}
			}
		});
	}

	private void loadFile() {
		FileDialog dialog = new FileDialog(sShell, SWT.OPEN);
		String result = dialog.open();
		if (result != null) {
			File f = new File(result);
			try {
				BufferedReader br = new BufferedReader(new FileReader(f));
				StringBuffer buff = new StringBuffer();
				String line = br.readLine();
				while (line != null) {
					buff.append(line + NEW_LINE);
					line = br.readLine();
				}
				// textArea.setText(buff.toString());
				br.close();
				sShell.setText(title);
				hasChanged = false;
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void saveFile() {

	}

	private boolean doOK () {
		if (hasChanged) {
			MessageBox mb = new MessageBox(sShell, SWT.ICON_QUESTION | SWT.YES
					| SWT.NO | SWT.CANCEL);
			mb.setText("Save Changes?");
			mb.setMessage("File has been changed. Save before exit?");
			int state = mb.open();
			if (state == SWT.YES) {
				// saveFile();
			} else if (state == SWT.CANCEL) {
				return false;
			}
		} 
		       // -------- 
		 if (radioButton1.getSelection()) dud.SELECT_CHANGE = 0;
		 if (radioButton2.getSelection()) dud.SELECT_CHANGE = 1;
		 if (radioButton3.getSelection()) dud.SELECT_CHANGE = 2;
		 dud.topIzdItemRevId = tfCodeIzd.getText() + "/" + tfCodeRevIzd.getText();
	    	dud.numberyl = tfLkcIzdNum.getText() + "/" + tfLkcRevIzdNum.getText();
	    	dud.vypolnil = tfRazrab.getText();
	    	dud.izdelie = tfIzdelieMC.getText();
		 dud.okFlag = true;
		       // ----------
		isClosing = true;
		sShell.close();
		sShell.dispose();
		return true;
	}

	private boolean doExit() {
		if (hasChanged) {
			MessageBox mb = new MessageBox(sShell, SWT.ICON_QUESTION | SWT.YES
					| SWT.NO | SWT.CANCEL);
			mb.setText("Save Changes?");
			mb.setMessage("File has been changed. Save before exit?");
			int state = mb.open();
			if (state == SWT.YES) {
				// saveFile();
			} else if (state == SWT.CANCEL) {
				return false;
			}
		}
		dud.okFlag = false;
		    // ----------------
		isClosing = true;
		sShell.close();
		sShell.dispose();
		return true;
	}




	/**
	 * This method initializes composite1	
	 *
	 */
	private void createComposite1() {
		GridLayout gridLayout = new GridLayout();
		composite1 = new Composite(sShell, SWT.NONE);
		composite1.setForeground(new Color(Display.getCurrent(), 4, 0, 244));
		composite1.setLayout(gridLayout);
		composite1.setBounds(new Rectangle(25, 4, 328, 33));
		label1 = new Label(composite1, SWT.NONE);
		label1.setText("���������");
		label1.setForeground(new Color(Display.getCurrent(), 91, 0, 0));
	}
}
